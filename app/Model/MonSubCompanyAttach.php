<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MonSubCompanyAttach extends Model
{
    protected $table = "mon_sub_companyattach";
	public $timestamps = false;
}
