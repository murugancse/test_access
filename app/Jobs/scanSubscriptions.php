<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\CommonHelper;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Fee;
use App\User;
use App\Model\Relation;
use App\Model\Race;
use App\Model\Reason;
use App\Model\Persontitle;
use App\Model\UnionBranch;
use App\Model\AppForm;
use App\Model\CompanyBranch;
use App\Model\Designation;
use App\Model\Status;
use App\Model\FormType;
use App\Model\Company;
use App\Mail\UnionBranchMailable;
use App\Mail\CompanyBranchMailable;
use App\Model\MonthlySubscription;
use App\Model\MonthlySubscriptionCompany;
use App\Model\MonthlySubscriptionMember;
use App\Model\MonSubCompanyAttach;
use App\Model\MonthlySubMemberMatch;
use App\Model\Membership;
use App\Model\ArrearEntry;
use DB;
use View;
use Mail;
use App\Role;
use URL;
use Response;
use App\Exports\SubscriptionExport;
use App\Imports\SubscriptionImport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\ToArray;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Log;
use Auth;


class scanSubscriptions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->limit = 25;
        ini_set('memory_limit', -1);
        //$this->middleware('auth');
        //$this->middleware('module:master');       
        $this->Company = new Company;
        $this->MonthlySubscription = new MonthlySubscription;
        $this->MonthlySubscriptionMember = new MonthlySubscriptionMember;
        $this->Status = new Status;
        $this->membermonthendstatus_table = "membermonthendstatus1";
        $this->ArrearEntry = new ArrearEntry;
        $bf_amount = Fee::where('fee_shortcode','=','BF')->pluck('fee_amount')->first();
        $ins_amount = Fee::where('fee_shortcode','=','INS')->pluck('fee_amount')->first();
        $this->bf_amount = $bf_amount=='' ? 0 : $bf_amount;
        $this->ins_amount = $ins_amount=='' ? 0 : $ins_amount;
        Log::info('test one');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         $subscription_data = MonthlySubscriptionMember::select('id','NRIC as ICNO','NRIC as NRIC','Name','Amount','MonthlySubscriptionCompanyId')
                                                            //->where('MonthlySubscriptionCompanyId',$company_auto_id)
                                                            ->where('update_status','=',0)
                                                            //->offset(0)
                                                            ->limit(1000)
                                                            ->get();
            
            $row_count = count($subscription_data);
            $count =0;
            
            foreach($subscription_data as $subscription){
                $company_auto_id =  $subscription->MonthlySubscriptionCompanyId;
                $nric = $subscription->NRIC;
               
                $subscription_new_qry =  DB::table('membership as m')->where('m.new_ic', '=',$nric)->OrderBy('m.doj','desc')->limit(1);
                
                $subscription_old_qry =  DB::table('membership as m')->where('m.old_ic', '=',$nric)->OrderBy('m.doj','desc')->limit(1);
                
                $subscription_empid_qry =  DB::table('membership as m')->where('m.employee_id', '=',$nric)->OrderBy('m.doj','desc')->limit(1);
                
                $up_sub_member =0;
                $match_count =  MonthlySubMemberMatch::where('mon_sub_member_id', '=',$subscription->id)
                                    ->where(function($query) {
                                          $query->where('match_id', 1)
                                            ->orWhere('match_id', 8)
                                            ->orWhere('match_id', 9)
                                            ->orWhere('match_id', 2);
                                      })->count();
                if($match_count>0){
                    $match_res =  MonthlySubMemberMatch::where('mon_sub_member_id', '=',$subscription->id)
                                        ->where(function($query) {
                                          $query->where('match_id', 1)
                                            ->orWhere('match_id', 8)
                                            ->orWhere('match_id', 9)
                                            ->orWhere('match_id', 2);
                                        })->get();
                    $matchid = $match_res[0]->id;
                    $subMemberMatch = MonthlySubMemberMatch::find($matchid);
                }else{
                    $subMemberMatch = new MonthlySubMemberMatch();
                }
                
                $subMemberMatch->mon_sub_member_id = $subscription->id;
                //dd(2);
                $subMemberMatch->created_by = 1;
                $subMemberMatch->created_on = date('Y-m-d');
               // DB::enableQueryLog();
                $insert_month_end = 0;
                $nric_matched = 0;
                if($subscription_new_qry->count() > 0){
                   
                    $memberdata = $subscription_new_qry->select('status_id','id','branch_id','name')->get();
                    $up_sub_member =1;
                    $subMemberMatch->match_id = 1;
                    $nric_matched = 1;
                   
                }else if($subscription_old_qry->count() > 0){
                    
                    $up_sub_member =1;
                    $memberdata = $subscription_old_qry->select('status_id','id','branch_id','name')->get();
                    $subMemberMatch->match_id = 8;
                    //$nric_matched = 1;
                }
                else if($subscription_empid_qry->count() > 0){
                    
                    $up_sub_member =1;
                    $memberdata = $subscription_empid_qry->select('status_id','id','branch_id','name')->get();
                    $subMemberMatch->match_id = 9;
                }
               
                else{
                    $subMemberMatch->match_id = 2;
                }
                $subMemberMatch->save();
              
                $upstatus=1;
                if($up_sub_member ==1){
                    $insert_month_end = 1;
                    if(count($memberdata)>0){
                        $status_id = $memberdata[0]->status_id;
                        $member_code = $memberdata[0]->id;
                        $updata = ['MemberCode' => $member_code, 'StatusId' => $status_id, 'update_status' => 1];
                        //DB::enableQueryLog();
                        $savedata = MonthlySubscriptionMember::where('MonthlySubscriptionCompanyId',$company_auto_id)
                        ->where('NRIC',$nric)->update($updata);
                        $upstatus=0;
                    }
                    
                    $company_code = CommonHelper::getcompanyidOfsubscribeCompanyid($subscription->MonthlySubscriptionCompanyId);
                    $member_company_id = CommonHelper::getcompanyidbyBranchid($memberdata[0]->branch_id);
                    
                    $match_company_status = $this->recursiveCompany($company_code,$member_company_id);
                
                    if(!$match_company_status){
                        $subMemberMatch_one = MonthlySubMemberMatch::where('match_id','=',4)->where('mon_sub_member_id','=',$subscription->id)->first();
                        
                        if(empty($subMemberMatch_one)){
                            $subMemberMatch_one = new MonthlySubMemberMatch();
                        }
                        $subMemberMatch_one->match_id = 4;
                        $subMemberMatch_one->mon_sub_member_id = $subscription->id;
                        $subMemberMatch_one->created_by = 1;
                        $subMemberMatch_one->created_on = date('Y-m-d');
                        $subMemberMatch_one->save();
                        $insert_month_end = 0;
                    }
                    
                                       
                    if(strtoupper(trim($memberdata[0]->name)) != strtoupper($subscription->Name)){
                        $subMemberMatch_two = MonthlySubMemberMatch::where('match_id','=',3)->where('mon_sub_member_id','=',$subscription->id)->first();
                        if(empty($subMemberMatch_two)){
                            $subMemberMatch_two = new MonthlySubMemberMatch();
                        }
                        $subMemberMatch_two->match_id = 3;
                        $subMemberMatch_two->mon_sub_member_id = $subscription->id;
                        $subMemberMatch_two->created_by = 1;
                        $subMemberMatch_two->created_on = date('Y-m-d');
                        $subMemberMatch_two->save();
                        $insert_month_end = 0;
                    }
                    
                    $cur_date = DB::table("mon_sub_company as mc")->leftjoin('mon_sub as ms','mc.MonthlySubscriptionId','=','ms.id')->where('mc.id','=',$subscription->MonthlySubscriptionCompanyId)->pluck('Date')->first();
                    $last_month = date('Y-m-01',strtotime($cur_date.' -1 Month'));

                    $old_subscription_count = DB::table("mon_sub_member as mm")
                            ->leftjoin('mon_sub_company as mc','mm.MonthlySubscriptionCompanyId','=','mc.id')
                            ->leftjoin('mon_sub as ms','mc.MonthlySubscriptionId','=','ms.id')
                            ->where('mm.MemberCode','=',$member_code)
                            ->where('ms.Date','=',$last_month)
                            ->orderBY('mm.MonthlySubscriptionCompanyId','desc')
                            ->count();
                            
                    $member_doj = DB::table("membership as m")->where('m.id','=',$member_code)->pluck('doj')->first();
                    $member_month_yr = date('m-Y',strtotime($member_doj));
                    $cur_month_yr = date('m-Y',strtotime($cur_date));
                    //dd($member_month_yr);
                    //dd($old_subscription_count);
                    if($member_month_yr!=$cur_month_yr){
                        if($old_subscription_count>0){
                            $old_subscription_amount = DB::table("mon_sub_member as mm")
                                ->leftjoin('mon_sub_company as mc','mm.MonthlySubscriptionCompanyId','=','mc.id')
                                ->leftjoin('mon_sub as ms','mc.MonthlySubscriptionId','=','ms.id')
                                ->where('mm.MemberCode','=',$member_code)
                                ->where('ms.Date','=',$last_month)
                                ->orderBY('mm.MonthlySubscriptionCompanyId','desc')
                                ->pluck('Amount')
                                ->first();
                            //dd($old_subscription_amount);
                            if($old_subscription_amount!=$subscription->Amount){
                                $subMemberMatch_three = MonthlySubMemberMatch::where('match_id','=',5)->where('mon_sub_member_id','=',$subscription->id)->first();
                                if(empty($subMemberMatch_three)){
                                    $subMemberMatch_three = new MonthlySubMemberMatch();
                                }
                                $subMemberMatch_three->match_id = 5;
                                $subMemberMatch_three->mon_sub_member_id = $subscription->id;
                                $subMemberMatch_three->created_by = 1;
                                $subMemberMatch_three->created_on = date('Y-m-d');
                                $subMemberMatch_three->save();
                                $insert_month_end = 0;
                            }
                        }else{
                            $subMemberMatch_four = MonthlySubMemberMatch::where('match_id','=',10)->where('mon_sub_member_id','=',$subscription->id)->first();
                            if(empty($subMemberMatch_four)){
                                $subMemberMatch_four = new MonthlySubMemberMatch();
                            }
                            
                            $subMemberMatch_four->match_id = 10;
                            $subMemberMatch_four->mon_sub_member_id = $subscription->id;
                            $subMemberMatch_four->created_by = 1;
                            $subMemberMatch_four->created_on = date('Y-m-d');
                            $subMemberMatch_four->save();
                            $insert_month_end = 0;
                        }
                    }
               
                    
                    if($memberdata[0]->status_id ==3){
                        $subMemberMatch_five = MonthlySubMemberMatch::where('match_id','=',6)->where('mon_sub_member_id','=',$subscription->id)->first();
                        if(empty($subMemberMatch_five)){
                            $subMemberMatch_five = new MonthlySubMemberMatch();
                        }
                        $subMemberMatch_five->match_id = 6;
                        $subMemberMatch_five->mon_sub_member_id = $subscription->id;
                        $subMemberMatch_five->created_by = 1;
                        $subMemberMatch_five->created_on = date('Y-m-d');
                        $subMemberMatch_five->save();
                        $insert_month_end = 0;
                    }else if($memberdata[0]->status_id ==4){
                        $subMemberMatch_six = MonthlySubMemberMatch::where('match_id','=',7)->where('mon_sub_member_id','=',$subscription->id)->first();
                        if(empty($subMemberMatch_six)){
                            $subMemberMatch_six = new MonthlySubMemberMatch();
                        }
                        $subMemberMatch_six->match_id = 7;
                        $subMemberMatch_six->mon_sub_member_id = $subscription->id;
                        $subMemberMatch_six->created_by = 1;
                        $subMemberMatch_six->created_on = date('Y-m-d');
                        $subMemberMatch_six->save();
                        $insert_month_end = 0;
                    }
                    
                    if($insert_month_end==1 && $nric_matched==1){
                        
                        $total_subs_obj = DB::table('mon_sub_member')->select(DB::raw('IFNULL(sum("Amount"),0) as amount'))
                        ->where('MemberCode', '=', $member_code)
                        ->first();
                        $total_subs = $total_subs_obj->amount;
                        
                        $total_count = DB::table('mon_sub_member')
                        ->where('MemberCode', '=', $member_code)
                        ->count();
                        
                        $paid_bf = $total_subs-($total_count*$this->bf_amount);
                        
                            
                        $to = Carbon::createFromFormat('Y-m-d H:s:i', $member_doj.' 3:30:34');
                        $from = Carbon::createFromFormat('Y-m-d H:s:i', $cur_date.' 9:30:34');
                        $diff_in_months = $to->diffInMonths($from);
                        
                        $bf_due = ($diff_in_months-$total_count)*$this->bf_amount;
                        $ins_due = ($diff_in_months-$total_count)*$this->ins_amount;
                        $total_subs_to_paid = $diff_in_months==0 ? $subscription->Amount : ($diff_in_months*$subscription->Amount);
                        $total_pending = $total_subs_to_paid - $total_subs;
                        
                        $mont_count = DB::table($this->membermonthendstatus_table)->where('StatusMonth', '=', $cur_date)->where('MEMBER_CODE', '=', $member_code)->count();
                        //dd($member_code);
                        $monthend_data = [
                                                'StatusMonth' => $cur_date, 
                                                'MEMBER_CODE' => $member_code,
                                                'SUBSCRIPTION_AMOUNT' => $subscription->Amount,
                                                'BF_AMOUNT' => $this->bf_amount,
                                                'LASTPAYMENTDATE' => $old_subscription_count>0 ? $last_month : NULL,
                                                'TOTALSUBCRP_AMOUNT' => $total_subs,
                                                'TOTALBF_AMOUNT' => $total_count*$this->bf_amount,
                                                'TOTAL_MONTHS' => $diff_in_months,
                                                //'ENTRYMODE' => 0,
                                                //'DEFAULTINGMONTHS' => 0,
                                                'TOTALMONTHSDUE' => $diff_in_months==0 ? 0 : $diff_in_months-$total_count,
                                                'TOTALMONTHSPAID' => $total_count,
                                                'SUBSCRIPTIONDUE' => $total_pending,
                                                'BFDUE' => $bf_due,
                                                'ACCSUBSCRIPTION' => $subscription->Amount,
                                                'ACCBF' => $this->bf_amount,
                                                //'ACCBENEFIT' => 0,
                                                //'CURRENT_YDTBF' => 0,
                                                //'CURRENT_YDTSUBSCRIPTION' => 0,
                                                'STATUS_CODE' => $memberdata[0]->status_id,
                                                'RESIGNED' => $memberdata[0]->status_id==4 ? 1 : 0,
                                                'ENTRY_DATE' => date('Y-m-d'),
                                                'ENTRY_TIME' => date('h:i:s'),
                                                'STRUCKOFF' => $memberdata[0]->status_id==3 ? 1 : 0,
                                                'INSURANCE_AMOUNT' => $this->ins_amount,
                                                'TOTALINSURANCE_AMOUNT' => $diff_in_months==0 ? $this->ins_amount : ($diff_in_months*$this->ins_amount),
                                                'TOTALMONTHSCONTRIBUTION' => $total_count,
                                                'INSURANCEDUE' => $ins_due,
                                                'ACCINSURANCE' => $this->ins_amount,
                                                //'CURRENT_YDTINSURANCE' => 0,
                                            ];
                        if($mont_count>0){
                            DB::table($this->membermonthendstatus_table)->where('StatusMonth', $cur_date)->where('MEMBER_CODE', $member_code)->update($monthend_data);
                        }else{
                            DB::table($this->membermonthendstatus_table)->insert($monthend_data);
                        }
                        
                    }
                }

               
                if( $upstatus==1){
                    $updata = ['update_status' => 1];
                    $savedata = MonthlySubscriptionMember::where('id',$subscription->id)->update($updata);
                }
                
                $count++;
            }
            //Log::info($start.'count-'.$count);
            //Log::Error($start.'rowcount-'.$row_count);
            //$enc_id = Crypt::encrypt($company_auto_id);
            //$return_data = ['status' => 1 ,'message' => 'status and member code updated successfully, Redirecting to subscription details...','redirect_url' =>  URL::to('/'.app()->getLocale().'/sub-company-members/'.$enc_id)];
        //return true;
    }

    public function recursiveCompany($company_code,$member_company_id){
        //Print out the number.
        //If the number is less or equal to 50.
        if($company_code == $member_company_id){
            //Call the function again. Increment number by one.
            return true;
        }else{
            $company_data = Company::find($member_company_id);
            if($company_code == $company_data->head_of_company){
                return true;
            }else{
                return false;
            }
        }
    }
}
