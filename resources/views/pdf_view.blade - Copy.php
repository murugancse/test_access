<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>{{ $title }}</title>
</head>
<body>
	<div style="position:absolute;left:50%;margin-left:-297px;top:0px;width:595px;height:841px;border-style:outset;overflow:hidden">
		<div style="position:absolute;left:0px;top:0px">
			<div style="position:absolute;left:279.53px;top:67.56px" class="cls_003"><span class="cls_003">N.U.B.E</span></div>
			<div style="position:absolute;left:261.24px;top:89.29px" class="cls_004"><span class="cls_004">12,NUBE HOUSE</span></div>
			<div style="position:absolute;left:231.80px;top:107.44px" class="cls_004"><span class="cls_004">JALAN TUN SAMBANTHAN3,</span></div>
			<div style="position:absolute;left:267.87px;top:125.29px" class="cls_004"><span class="cls_004">BRICKFIELDS</span></div>
			<div style="position:absolute;left:261.24px;top:143.29px" class="cls_004"><span class="cls_004">KUALA LUMPUR</span></div>
			<div style="position:absolute;left:275.43px;top:161.15px" class="cls_004"><span class="cls_004">MALAYSIA</span></div>
			<div style="position:absolute;left:73.61px;top:179.29px" class="cls_002"><span class="cls_002">PAYMENT VOUCHER</span></div>
			<div style="position:absolute;left:232.73px;top:196.50px" class="cls_002"><span class="cls_002">N.U.B.E BENEVOLENT FUND</span></div>
			<div style="position:absolute;left:73.61px;top:223.35px" class="cls_005"><span class="cls_005">MEMBER NAME</span></div>
			<div style="position:absolute;left:166.61px;top:223.35px" class="cls_005"><span class="cls_005">:</span></div>
			<div style="position:absolute;left:179.67px;top:223.35px" class="cls_005"><span class="cls_005">AHMAD JOHARI BIN ALI</span></div>
			<div style="position:absolute;left:411.87px;top:224.22px" class="cls_002"><span class="cls_002">DATE :</span></div>
			<div style="position:absolute;left:450.17px;top:224.22px" class="cls_002"><span class="cls_002">15-11-2018</span></div>
			<div style="position:absolute;left:73.61px;top:239.91px" class="cls_005"><span class="cls_005">MEMBERSHIP NO</span></div>
			<div style="position:absolute;left:165.11px;top:239.91px" class="cls_005"><span class="cls_005">:</span></div>
			<div style="position:absolute;left:179.67px;top:240.78px" class="cls_002"><span class="cls_002">39473</span></div>
			<div style="position:absolute;left:223.80px;top:240.78px" class="cls_002"><span class="cls_002">BANK CODE:</span></div>
			<div style="position:absolute;left:289.11px;top:240.78px" class="cls_002"><span class="cls_002">CIMB/ 128</span></div>
			<div style="position:absolute;left:363.99px;top:242.29px" class="cls_002"><span class="cls_002">BRANCH CODE :</span></div>
			<div style="position:absolute;left:450.17px;top:242.29px" class="cls_002"><span class="cls_002">01</span></div>
			<div style="position:absolute;left:73.61px;top:257.40px" class="cls_005"><span class="cls_005">CLAIMER NAME</span></div>
			<div style="position:absolute;left:167.12px;top:257.40px" class="cls_005"><span class="cls_005">:</span></div>
			<div style="position:absolute;left:179.67px;top:256.48px" class="cls_002"><span class="cls_002">AHMAD JOHARI BIN ALI</span></div>
			<div style="position:absolute;left:410.57px;top:260.29px" class="cls_002"><span class="cls_002">IC NO :</span></div>
			<div style="position:absolute;left:450.82px;top:260.29px" class="cls_002"><span class="cls_002">640402085189</span></div>
			<div style="position:absolute;left:73.61px;top:274.83px" class="cls_005"><span class="cls_005">RESIGNATION DATE :</span></div>
			<div style="position:absolute;left:179.67px;top:273.90px" class="cls_002"><span class="cls_002">01-07-2015</span></div>
			<div style="position:absolute;left:73.61px;top:292.40px" class="cls_005"><span class="cls_005">REASON FOR CLAIM :</span><span class="cls_002">   MUTUAL SEPARATION SCHEME</span></div>
			<div style="position:absolute;left:91.61px;top:321.28px" class="cls_002"><span class="cls_002">REFUND OF CONTRIBUTIONS :</span></div>
			<div style="position:absolute;left:386.09px;top:319.55px" class="cls_002"><span class="cls_002">AMOUNT</span></div>
			<div style="position:absolute;left:470.69px;top:320.27px" class="cls_002"><span class="cls_002">TOTAL</span></div>
			<div style="position:absolute;left:175.21px;top:339.28px" class="cls_002"><span class="cls_002">PAID FROM 28-Feb-1987 TO 30/06/2015</span></div>
			<div style="position:absolute;left:483.94px;top:339.28px" class="cls_002"><span class="cls_002">2013.00</span></div>
			<div style="position:absolute;left:188.96px;top:357.64px" class="cls_002"><span class="cls_002">@RM 3 PER MONTH (RM 3 x341.00 )</span></div>
			<div style="position:absolute;left:401.29px;top:357.64px" class="cls_002"><span class="cls_002">1023.00</span></div>
			<div style="position:absolute;left:149.21px;top:376.07px" class="cls_002"><span class="cls_002">BENEFIT PAYABLE FROM UNION : 28YEARS</span></div>
			<div style="position:absolute;left:406.83px;top:376.07px" class="cls_002"><span class="cls_002">990.00</span></div>
			<div style="position:absolute;left:281.19px;top:399.83px" class="cls_002"><span class="cls_002">Insurance Amount</span></div>
			<div style="position:absolute;left:417.99px;top:398.39px" class="cls_002"><span class="cls_002">0.00</span></div>
			<div style="position:absolute;left:73.61px;top:441.23px" class="cls_002"><span class="cls_002">Payment Authorized By</span></div>
			<div style="position:absolute;left:426.27px;top:441.23px" class="cls_002"><span class="cls_002">Payment Make By</span></div>
			<div style="position:absolute;left:111.99px;top:528.64px" class="cls_002"><span class="cls_002">President</span></div>
			<div style="position:absolute;left:260.96px;top:528.64px" class="cls_002"><span class="cls_002">Hon Gen Secretary</span></div>
			<div style="position:absolute;left:439.52px;top:528.64px" class="cls_002"><span class="cls_002">Hon Treasurer</span></div>
		</div>
		<div style="position:absolute;left:50%;margin-left:-297px;top:851px;width:595px;height:841px;border-style:outset;overflow:hidden">
			<div style="position:absolute;left:0px;top:0px">
				<img src="4bf36fb8-c2f9-11e9-9d71-0cc47a792c0a_id_4bf36fb8-c2f9-11e9-9d71-0cc47a792c0a_files/background2.jpg" width="595" height="841"/>
			</div>
		</div>
	</div>
</body>
</body>
</html>