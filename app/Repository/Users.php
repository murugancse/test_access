<?php

namespace App\Repository;

use App\User;
use Carbon\Carbon;
use Cache;

class Users
{
	CONST CACHE_KEY="USERS";

	public function all($orderBy){
		$key = "all.{$orderBy}";
		$cacheKey = $this->getCacheKey($key);
		return Cache::remember($cacheKey,Carbon::now()->addMinutes(5), function() use($cacheKey,$orderBy)
		{
		    return User::orderBy($orderBy)->limit(10000)->get();
		});
		

	}

	public function get($id){
		$key = "get.{$id}";
		$cacheKey = $this->getCacheKey($key);
		return Cache::remember($cacheKey,Carbon::now()->addMinutes(5), function() use($id)
		{
		    return User::find($id);
		});
		

	}


	public function getCacheKey($key){
		$key = strtoupper($key);
		return self::CACHE_KEY.".$key";
	}
}